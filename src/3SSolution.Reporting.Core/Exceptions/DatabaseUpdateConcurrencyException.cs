﻿using System;

namespace _3SSolution.Reporting.Core.Exceptions
{
    public class DatabaseUpdateConcurrencyException : Exception
    {
        public DatabaseUpdateConcurrencyException(string message) : base(message)
        {
        }
    }
}
