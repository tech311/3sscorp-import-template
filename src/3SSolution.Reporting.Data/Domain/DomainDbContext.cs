﻿using Microsoft.EntityFrameworkCore;

namespace _3SSolution.Reporting.Data.Domain
{
    public class DomainDbContext : DbContext
    {
        public DomainDbContext(DbContextOptions<DomainDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            // applying configuration to entity
            // ex: builder.Entity<Customer>();
        }

        // declare DbSet
        // ex: public DbSet<Customer> Customers { get; set; }
    }
}