﻿namespace _3SSolution.Reporting.Core.Entities
{
    public interface ISoftDelete
    {
        bool? IsDeleted { get; set; }
    }
}