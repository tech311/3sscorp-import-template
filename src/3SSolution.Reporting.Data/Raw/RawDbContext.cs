﻿using Microsoft.EntityFrameworkCore;

namespace _3SSolution.Reporting.Data.Raw
{
    public class RawDbContext : DbContext
    {
        public RawDbContext(DbContextOptions<RawDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            // applying configuration to entity
            // ex: builder.Entity<Customer>();
        }

        // declare DbSet 
        // ex: public DbSet<Customer> Customers { get; set; }
    }
}