﻿using _3SSolution.Reporting.Core.Entities;
using _3SSolution.Reporting.Core.Specifications;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace _3SSolution.Reporting.Core.Data
{
    public interface IRepository<TEntity> where TEntity : BaseEntity, IAggregateRoot
    {
        Task<TEntity> GetByIdAsync(int id);

        Task<IReadOnlyList<TEntity>> ListAllAsync();

        Task<IReadOnlyList<TEntity>> ListAsync(ISpecification<TEntity> spec);

        Task<TEntity> AddAsync(TEntity entity);

        Task UpdateAsync(TEntity entity);

        Task DeleteAsync(TEntity entity);

        Task<int> CountAsync(ISpecification<TEntity> spec);
    }
}