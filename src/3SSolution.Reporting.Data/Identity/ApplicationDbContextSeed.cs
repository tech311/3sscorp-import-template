﻿using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace _3SSolution.Reporting.Data.Identity
{
    public class ApplicationDbContextSeed
    {
        public static async Task SeedAsync(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            // create administrator role
            ApplicationRole adminRole = new ApplicationRole { Name = "Administrator" };
            bool isExistedAdminRole = await roleManager.RoleExistsAsync(adminRole.Name);
            if (!isExistedAdminRole)
            {
                await roleManager.CreateAsync(adminRole);

                // create role claims, all permission should be granted to super admin
                roleManager.AddClaimAsync(adminRole, new Claim(ApplicationClaimType.Permission, ApplicationClaimPolicy.UserCreate)).Wait();
                roleManager.AddClaimAsync(adminRole, new Claim(ApplicationClaimType.Permission, ApplicationClaimPolicy.UserRead)).Wait();
                roleManager.AddClaimAsync(adminRole, new Claim(ApplicationClaimType.Permission, ApplicationClaimPolicy.UserUpdate)).Wait();
                roleManager.AddClaimAsync(adminRole, new Claim(ApplicationClaimType.Permission, ApplicationClaimPolicy.UserDelete)).Wait();
            }

            // create super admin user
            ApplicationUser superAdminUser = new ApplicationUser
            {
                UserName = "sa@3ssolution.com.vn",
                Email = "sa@3ssolution.com.vn",
            };
            ApplicationUser existedSuperAdmin = await userManager.FindByEmailAsync(superAdminUser.Email);
            if (existedSuperAdmin == null)
            {
                await userManager.CreateAsync(superAdminUser, "Admin@123");
                // assign administrator role to super user
                await userManager.AddToRoleAsync(superAdminUser, adminRole.Name);
            }
        }
    }
}