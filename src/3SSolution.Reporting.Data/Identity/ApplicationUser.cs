﻿using Microsoft.AspNetCore.Identity;
using System;

namespace _3SSolution.Reporting.Data.Identity
{
    public class ApplicationUser : IdentityUser<Guid>
    {
    }
}