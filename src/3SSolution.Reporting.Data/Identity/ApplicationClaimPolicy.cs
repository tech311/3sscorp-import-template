﻿namespace _3SSolution.Reporting.Data.Identity
{
    public class ApplicationClaimPolicy
    {
        public const string UserCreate = "user.create";
        public const string UserRead = "user.read";
        public const string UserUpdate = "user.update";
        public const string UserDelete = "user.delete";
    }
}