﻿using _3SSolution.Reporting.Core.Data;
using _3SSolution.Reporting.Core.Entities;
using _3SSolution.Reporting.Core.Exceptions;
using _3SSolution.Reporting.Core.Specifications;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _3SSolution.Reporting.Data
{
    public class EfRepository<TEntity, TDbContext> : IRepository<TEntity>
        where TEntity : BaseEntity, IAggregateRoot
        where TDbContext : DbContext
    {
        protected readonly TDbContext _dbContext;
        private readonly string _updatedDateFieldName = "UpdatedDate";
        private readonly string _createdDateFieldName = "CreatedDate";
        private readonly string _updatedByFieldName = "UpdatedBy";
        private readonly string _createdByFieldName = "CreatedBy";

        public EfRepository(TDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual async Task<TEntity> GetByIdAsync(int id)
        {
            return await _dbContext.Set<TEntity>().FindAsync(id);
        }

        public async Task<IReadOnlyList<TEntity>> ListAllAsync()
        {
            return await _dbContext.Set<TEntity>().ToListAsync();
        }

        public async Task<IReadOnlyList<TEntity>> ListAsync(ISpecification<TEntity> spec)
        {
            return await ApplySpecification(spec).ToListAsync();
        }

        public async Task<int> CountAsync(ISpecification<TEntity> spec)
        {
            return await ApplySpecification(spec).CountAsync();
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            _dbContext.Set<TEntity>().Add(entity);
            if (entity is IAuditable)
            {
                var utcNow = DateTime.UtcNow;
                ((IAuditable)entity).CreatedDate = utcNow;
                ((IAuditable)entity).UpdatedDate = utcNow;
            }
            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public async Task UpdateAsync(TEntity entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            if (entity is IAuditable)
            {
                var originalUpdatedDate = _dbContext.Entry(entity).GetDatabaseValues().GetValue<object>(_updatedDateFieldName);
                var currentUpdatedDate = _dbContext.Entry(entity).Property(_updatedDateFieldName).CurrentValue;
                if ((int)((DateTime)originalUpdatedDate - (DateTime)currentUpdatedDate).TotalSeconds > 0)
                {
                    throw new DatabaseUpdateConcurrencyException("DbUpdateConcurrencyException");
                }

                _dbContext.Entry(entity).Property(_createdDateFieldName).IsModified = false;
                _dbContext.Entry(entity).Property(_createdByFieldName).IsModified = false;
                ((IAuditable)entity).UpdatedDate = DateTime.UtcNow;
            }
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(TEntity entity)
        {
            _dbContext.Set<TEntity>().Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        private IQueryable<TEntity> ApplySpecification(ISpecification<TEntity> spec)
        {
            return SpecificationEvaluator<TEntity>.GetQuery(_dbContext.Set<TEntity>().AsQueryable(), spec);
        }
    }
}